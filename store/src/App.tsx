import React, { useEffect } from "react";
import { useAppDispatch } from "./hooks/Hooks";
import { GamesFetch } from "./reduxStore/slices/GamesSlice";
import AppRoutes from "./components/routes/AppRoutes";

const App: React.FC = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(GamesFetch());
  }, [dispatch]);

  return (
    <>
      <AppRoutes />
    </>
  );
};

export default App;
