import { configureStore } from "@reduxjs/toolkit";
import GamesSlice from "../reduxStore/slices/GamesSlice";
import GamePageSlice from "./slices/GamePageSlice";
import BasketSlice from "./slices/AddToBasketSlice"
import SuccessAlertSlice from "./slices/SuccessAlertSlice"
import InfoAlertSlice from "./slices/InfoAlertSlice"

const store = configureStore({
  reducer: {
    games: GamesSlice,
    gamePage: GamePageSlice,
    basket: BasketSlice,
    successAlert: SuccessAlertSlice,
    infoAlertSlice: InfoAlertSlice,
  },
});

export default store;
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
