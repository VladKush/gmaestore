import { createSlice } from "@reduxjs/toolkit";
import { IGame } from "../../components/types/Types";

type userState = {
  basket: IGame[];
};

export const token = "auth0.M7nokbmQ18H0AIxX3wAAgp2xM6c8mfew.is.";

const initialState: userState = {
  basket: [],
};

const addToBasketSlice = createSlice({
  name: "basket",
  initialState,
  reducers: {
    addGameToBasketState(state, { payload }: { payload: IGame }) {
      state.basket.push(payload);
      // localStorage.setItem(token, JSON.stringify(state.basket));
    },
    removeFromBasketState(state, { payload }: { payload: number }) {
      state.basket = state.basket.filter((game) => game.id !== payload);
      // localStorage.setItem(token, JSON.stringify(state.basket));
    },
    // setBasketFromLocalStorage(state, { payload } : { payload: IGame[]}) {
    //     state.basket = payload
    // },
  },
});

export const {
  addGameToBasketState,
  removeFromBasketState,
  // setBasketFromLocalStorage,
} = addToBasketSlice.actions;

export default addToBasketSlice.reducer;
