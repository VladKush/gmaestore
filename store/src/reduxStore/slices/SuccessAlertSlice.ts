import { createSlice } from "@reduxjs/toolkit";

type userState = {
    successAlert: boolean;
};

const initialState: userState = {
    successAlert: false,
};

const SuccessAlertSlice = createSlice({
    name: "successAlert",
    initialState,
    reducers: {
        SuccessAlertState(state, { payload }: { payload: boolean }) {
            state.successAlert = payload;
        },

    },
});

export const { SuccessAlertState } = SuccessAlertSlice.actions;

export default SuccessAlertSlice.reducer;