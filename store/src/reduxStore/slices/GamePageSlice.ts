import { createSlice } from "@reduxjs/toolkit";
import { IGame } from "../../components/types/Types"

type userState = {
    gamePage: IGame;
};

const initialState: userState = {
    gamePage: {
        "image": "img/gta_v.jpeg",
        "title": "Grand Theft Auto V",
        "genres": ["Открытый мир", "Экшен"],
        "video": "https://www.youtube.com/embed/QkkoHAzjnUs",
        "price": 260,
        "id": 4,
        "description": "Grand Theft Auto V для PC позволяет игрокам исследовать знаменитый мир Лос-Сантоса и округа Блэйн в разрешении до 4k и выше с частотой 60 кадров в секунду."
    },
};

const gamePageSlice = createSlice({
  name: "gamePage",
  initialState,
  reducers: {
    addGamePageState(state, { payload }: { payload: IGame }) {
      state.gamePage = payload;
    },
  },
});

export const { addGamePageState } = gamePageSlice.actions;

export default gamePageSlice.reducer;