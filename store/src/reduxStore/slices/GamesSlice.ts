import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { IGame } from "../../components/types/Types";

type UserState = {
  games: IGame[];
  loading: boolean;
  error: null | string;
};

export const GamesFetch = createAsyncThunk<
  IGame[],
  undefined,
  { rejectValue: string }
>("GamesFetch", async function (_, { rejectWithValue }) {
  const response = await fetch("./games.json");
  try {
    if (!response.ok) {
      throw new Error("Server Error");
    }
    const data = response.json();

    return data;
  } catch (e) {
    return rejectWithValue(`${e}`);
  }
});

const initialState: UserState = {
  games: [],
  loading: false,
  error: null,
};

const gamesSlice = createSlice({
  name: "games",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(GamesFetch.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(GamesFetch.fulfilled, (state, action) => {
        state.games = action.payload;
        state.loading = false;
      });
  },
});

export default gamesSlice.reducer;
