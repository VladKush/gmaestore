import { createSlice } from "@reduxjs/toolkit";

type userState = {
    infoAlert: boolean;
};

const initialState: userState = {
    infoAlert: false,
};

const InfoAlertSlice = createSlice({
    name: "InfoAlert",
    initialState,
    reducers: {
        InfoAlertState(state, { payload }: { payload: boolean }) {
            state.infoAlert = payload;
        },

    },
});

export const { InfoAlertState } = InfoAlertSlice.actions;

export default InfoAlertSlice.reducer;