import React from "react";
import { useAppSelector } from "../../hooks/Hooks";
import { CardContent, Container, Grid, Typography } from "@mui/material";
import BasketItem from "./BasketItem";
import { Link } from "react-router-dom";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import "./BasketList.scss";

const BasketList: React.FC = () => {
  const basket = useAppSelector((state) => state.basket.basket);
  const totalPrice = basket.reduce((acc, game) => (acc += game.price), 0);
  console.log(basket);

  return (
    <Container>

      <div className='top-basket'>
        <Link to="/">
          <Typography color="primary">
            <div className="basket-back-arrow">
              <ArrowBackIcon />
              <span>На главную</span>
            </div>
          </Typography>
        </Link>

        {!basket.length ? (
          ""
        ) : (
          <Typography className='total-price' color="primary">Общая цена: {totalPrice}</Typography>
        )}
      </div>

      {!basket.length ? (
        <CardContent>
          <Typography
            sx={{ mt: 10 }}
            color="primary"
            component="h1"
            variant="h1"
          >
            Корзина пуста
          </Typography>
        </CardContent>
      ) : (
        <Grid container spacing={5}>
          {basket.map((game) => (
            <Grid key={game.id} className="basket" item xs={12} md={6} xl={4}>
              <BasketItem {...game} />
            </Grid>
          ))}
        </Grid>
      )}
    </Container>
  );
};

export default BasketList;
