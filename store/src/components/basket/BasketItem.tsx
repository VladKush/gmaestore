import React from "react";
import { IGame } from "../types/Types";
import {
  Box,
  Button,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import { useAppDispatch } from "../../hooks/Hooks";
import { removeFromBasketState } from "../../reduxStore/slices/AddToBasketSlice";
import { Link } from "react-router-dom";
import {InfoAlertState} from "../../reduxStore/slices/InfoAlertSlice";

const BasketItem: React.FC<IGame> = (props) => {
  const { title, price, image, id } = props;

  const dispatch = useAppDispatch();

  const deleteFromBasket = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.preventDefault();
    dispatch(removeFromBasketState(props.id));
    dispatch(InfoAlertState(true))
  };

  return (
    <Link to={`/gameId/${id}`}>
      <Box>
        <CardContent>

          <CardMedia
            sx={{ width: "300px", height: '180px' }}
            image={image}
            alt={title}
            title={title}
            component="img"
          />

          <Typography
            sx={{ height: "30px", overflow: "hidden", mt: 1 }}
            color="primary"
            gutterBottom
            variant="h6"
            component="div"
            dangerouslySetInnerHTML={{ __html: title }}
          >
          </Typography>

          <Typography sx={{mb: 1, mt: 1}} color="primary" variant="body2">
            Цера: {price} грн
          </Typography>

          <CardActions>
            <Button
              onClick={(e) => deleteFromBasket(e)}
              variant="outlined"
              color="error"
              startIcon={<DeleteIcon />}
            >
              Убрать из корзины
            </Button>
          </CardActions>

        </CardContent>
      </Box>
    </Link>
  );
};

export default BasketItem;
