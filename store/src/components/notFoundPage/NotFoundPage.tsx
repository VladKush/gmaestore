import React from "react";
import { Link } from "react-router-dom";
import {Container, Typography} from "@mui/material";

const NotFoundPage: React.FC = () => {
  return (
    <Container sx={{textAlign: 'center', mt: 20}}>
      <Typography color='error' variant='h3' component='div'>Something went wrong!</Typography>
      <Link to="/" className="text-black">
        <Typography color='primary' variant='h6' component='div'>Go to main page</Typography>
      </Link>
    </Container>
  );
};

export default NotFoundPage;
