import React from "react";
import { Button } from "@mui/material";

interface Props {
  setSortPriseButton: any;
  sortPriseButton: boolean | null;
}

const SortByPriceButton: React.FC<Props> = ({
  setSortPriseButton,
  sortPriseButton,
}: Props) => {
  const sortByPriceBtn = () => {
    setSortPriseButton((prevState: boolean) => !prevState);
  };

  return (
    <Button onClick={sortByPriceBtn} color="primary" variant="outlined">
      {sortPriseButton === null
        ? "Сортовать по цене"
        : !sortPriseButton
        ? "От дорогих"
        : "От дешёвых"}
    </Button>
  );
};

export default SortByPriceButton;
