import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import "./UserPage.scss";
import {Container, Typography} from "@mui/material";
import { useAppSelector } from "../../hooks/Hooks";
import { Link } from "react-router-dom";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

const UserPage: React.FC = () => {
  const { user, isAuthenticated } = useAuth0();
  const itemsInBasket = useAppSelector((state) => state.basket.basket);

  return (
    <Container>
      <Link to="/">
        <Typography color="primary">

          <div className='back-arrow'>
            <ArrowBackIcon />
            <div>На главную</div>
          </div>

        </Typography>
      </Link>
      {isAuthenticated && (
        <div className='user-page'>
          <div className='user-image-wrapper'>
            <img src={user?.picture} alt={user?.name} />
          </div>
          <h1>{user?.name}</h1>
          <Link to="/basketPage">
            <div className='user-page-basketItems'>
              {itemsInBasket.length === 0
                ? "Ваша корзина пуста"
                : `Товаров в корзине: ${itemsInBasket.length}`}
            </div>
          </Link>
        </div>
      )}
    </Container>
  );
};

export default UserPage;
