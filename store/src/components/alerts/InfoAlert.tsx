import React from 'react';
import {Alert} from "@mui/material";
import Snackbar from "@mui/material/Snackbar";

type PropsType = {
    isOpen: boolean,
    handleClose: any,
}

const InfoAlert: React.FC<PropsType> = ({isOpen, handleClose}) => {
    return (
        <Snackbar open={isOpen} autoHideDuration={2000} onClose={handleClose}>
            <Alert
                severity="info"
                sx={{ width: "100%" }}>
                Товар убран из корзинны!
            </Alert>
        </Snackbar>
    );
};

export default InfoAlert;