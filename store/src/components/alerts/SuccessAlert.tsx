import React from 'react';
import {Alert} from "@mui/material";
import Snackbar from "@mui/material/Snackbar";

type PropsType = {
    isOpen: boolean,
    handleClose: any,
}

const SuccessAlert: React.FC<PropsType> = ({isOpen, handleClose}) => {
    return (
        <Snackbar open={isOpen} autoHideDuration={2000} onClose={handleClose}>
            <Alert
                severity="success"
                sx={{ width: "100%" }}>
                Товар добавлен в корзину!
            </Alert>
        </Snackbar>
    );
};

export default SuccessAlert;