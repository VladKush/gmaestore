import React from "react";
import { Route, Routes } from "react-router-dom";
import Layout from "../layout/Header";
import CatalogList from "../catalog/CatalogList";
import NotFoundPage from "../notFoundPage/NotFoundPage";
import GamePage from "../gamePage/GamePage";
import BasketList from "../basket/BasketList";
import UserPage from "../userPage/UserPage";

const AppRoutes: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<CatalogList />} />
        <Route path="gameId/:gameId" element={<GamePage />} />
        <Route path="basketPage" element={<BasketList />} />
        <Route path="user/:userName" element={<UserPage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Route>
    </Routes>
  );
};

export default AppRoutes;
