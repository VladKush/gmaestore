import React from "react";
import { Link, Outlet } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks/Hooks";
import {
  AppBar,
  Badge,
  BadgeProps,
  IconButton,
  Stack,
  styled,
  Toolbar,
  Typography,
} from "@mui/material";
import SportsEsportsOutlinedIcon from "@mui/icons-material/SportsEsportsOutlined";
import { ShoppingCart } from "@mui/icons-material";
import AuthButton from "../auth/AuthButton";
import SuccessAlert from "../alerts/SuccessAlert";
import { SuccessAlertState } from "../../reduxStore/slices/SuccessAlertSlice";
import InfoAlert from "../alerts/InfoAlert";
import { InfoAlertState } from "../../reduxStore/slices/InfoAlertSlice";

const Header: React.FC = () => {
  const basketItems = useAppSelector((state) => state.basket.basket);
  let successAlert = useAppSelector((state) => state.successAlert.successAlert);
  let infoAlert = useAppSelector((state) => state.infoAlertSlice.infoAlert);
  const dispatch = useAppDispatch();

  const StyledBadge = styled(Badge)<BadgeProps>(() => ({
    "& .MuiBadge-badge": {
      right: 1,
      top: 5,
      padding: "0 4px",
    },
  }));

  return (
    <>
      <AppBar position="static" color="secondary">
        <Toolbar>
          <Link to="/">
            <IconButton color="primary" aria-label="logo" edge="start">
              <SportsEsportsOutlinedIcon style={{ fontSize: 50 }} />
            </IconButton>
          </Link>

          <Typography
            color="primary"
            variant="h4"
            component="div"
            sx={{ flexGrow: 1 }}
          >
            Game store
          </Typography>

          <Stack direction="row" spacing={2}>
            <AuthButton />

            <Link to="/basketPage">
              <IconButton color="primary">
                <StyledBadge badgeContent={basketItems.length} color={"error"}>
                  <ShoppingCart />
                </StyledBadge>
              </IconButton>
            </Link>
          </Stack>
        </Toolbar>
      </AppBar>

      <Outlet />

      <SuccessAlert
        isOpen={successAlert}
        handleClose={() => dispatch(SuccessAlertState(false))}
      />
      <InfoAlert
        isOpen={infoAlert}
        handleClose={() => dispatch(InfoAlertState(false))}
      />
    </>
  );
};

export default Header;
