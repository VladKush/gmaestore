import React, { useState } from "react";
import {
  Avatar,
  Box,
  IconButton,
  Menu,
  Tooltip,
  Typography,
} from "@mui/material";
import MenuItem from "@mui/material/MenuItem";
import { useAuth0 } from "@auth0/auth0-react";
import { Link } from "react-router-dom";

const NavUserMenu: React.FC = () => {
  const { user, logout } = useAuth0();
  const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);

  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <Box sx={{ flexGrow: 0 }}>
      <Tooltip title="Open settings">
        <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
          <Avatar alt="Remy Sharp" src={`${user?.picture}`} />
        </IconButton>
      </Tooltip>
      <Menu
        sx={{ mt: "45px" }}
        id="menu-appbar"
        anchorEl={anchorElUser}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={Boolean(anchorElUser)}
        onClose={handleCloseUserMenu}
      >
        <MenuItem onClick={handleCloseUserMenu}>
          <Link to={`/user/${user?.name}`}>
            <Typography color="primary" textAlign="center">
              Account
            </Typography>
          </Link>
        </MenuItem>
        <MenuItem onClick={handleCloseUserMenu}>
          <Link to="/basketPage">
            <Typography color="primary" textAlign="center">
              Basket
            </Typography>
          </Link>
        </MenuItem>
        <MenuItem onClick={handleCloseUserMenu}>
          <Typography
            color="primary"
            onClick={() => logout()}
            textAlign="center"
          >
            Logout
          </Typography>
        </MenuItem>
      </Menu>
    </Box>
  );
};

export default NavUserMenu;
