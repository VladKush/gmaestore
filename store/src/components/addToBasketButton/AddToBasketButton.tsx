import React from "react";
import { IGame } from "../types/Types";
import { useAppDispatch, useAppSelector } from "../../hooks/Hooks";
import {
  addGameToBasketState,
  removeFromBasketState,
} from "../../reduxStore/slices/AddToBasketSlice";
import { Button, CardActions } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import {SuccessAlertState} from "../../reduxStore/slices/SuccessAlertSlice";
import {InfoAlertState} from "../../reduxStore/slices/InfoAlertSlice";

const AddToBasketButton: React.FC<IGame> = (props) => {
  const dispatch = useAppDispatch();
  const basket = useAppSelector((state) => state.basket.basket);
  const isGameInBasket = basket.some(
    (gameInBasket) => gameInBasket.id === props.id
  );

  const addToBasketHandler = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.preventDefault();
    if (isGameInBasket) {
      dispatch(removeFromBasketState(props.id));
      dispatch(InfoAlertState(true))
    } else {
      dispatch(addGameToBasketState(props));
      dispatch(SuccessAlertState(true))
    }
  };

  return (
    <CardActions>
      {!isGameInBasket ? (
        <Button
          onClick={(e) => addToBasketHandler(e)}
          variant="contained"
          color="primary"
        >
          Добавить в корзину
        </Button>
      ) : (
        <Button
          onClick={(e) => addToBasketHandler(e)}
          variant="outlined"
          color="error"
          startIcon={<DeleteIcon />}
        >
          Убрать из корзины
        </Button>
      )}
    </CardActions>
  );
};

export default AddToBasketButton;
