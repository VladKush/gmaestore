import React from "react";
import ReactPaginate from "react-paginate";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import { IPaginationProps } from "../types/Types";
import "./Pagination.scss";

const Pagination: React.FC<IPaginationProps> = ({
  // initialPage,
  pageCount,
  onChange,
                                                  forcePage,
}) => {
  return (
    <ReactPaginate
      // initialPage={initialPage}
      pageCount={pageCount}
      onPageChange={onChange}
      forcePage={forcePage}
      containerClassName="Pagination"
      activeClassName="Pagination__active"
      previousLabel={<ArrowBackIosNewIcon color="primary" />}
      nextLabel={<ArrowForwardIosIcon color="primary" />}
    />
  );
};

export default Pagination;
