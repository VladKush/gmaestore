export interface IGame {
  image: string;
  title: string;
  genres: string[];
  price: number;
  video: string;
  id: number;
  description: string;
}

export interface IPaginationProps {
  pageCount: number;
  forcePage: number
  onChange: ({ selected }: { selected: number }) => void;
}