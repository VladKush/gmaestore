import React from "react";
import { TextField } from "@mui/material";

interface Props {
  setSearchGameInp: any;
}

const SearchInput: React.FC<Props> = ({ setSearchGameInp }: Props) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchGameInp(event.target.value);
  };

  return (
    <TextField
      color="primary"
      variant="standard"
      label="Назва игры"
      id="standard-basic"
      focused
      onChange={handleChange}
    />
  );
};

export default SearchInput;
