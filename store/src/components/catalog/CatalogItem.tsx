import React from "react";
import { IGame } from "../types/Types";
import { useAppDispatch } from "../../hooks/Hooks";
import { addGamePageState } from "../../reduxStore/slices/GamePageSlice";
import { Link } from "react-router-dom";
import AddToBasketButton from "../addToBasketButton/AddToBasketButton";
import {
  CardContent,
  CardMedia,
  Typography,
  CardActions,
  Box,
} from "@mui/material";

const CatalogItem: React.FC<IGame> = (props) => {
  const { title, image, genres, price, id } = props;
  const dispatch = useAppDispatch();

  return (
    <Link
      onClick={() => dispatch(addGamePageState(props))}
      to={`gameId/${id}`}
    >
      <Box width="360px">
        <CardMedia
          sx={{ height: "250px" }}
          image={image}
          alt={title}
          title={title}
          component="img"
        />
        <CardContent>
            <Typography
                color="primary"
                variant="h5"
                component="div"
                sx={{ height: 33, overflow: "hidden" }}
                dangerouslySetInnerHTML={{ __html: title }}
            >
            </Typography>
            <Typography color="primary" variant="body2">
              Жанр: {genres.join(", ")}
            </Typography>
            <Typography color="primary" variant="h6" component="div">
              {price} грн
            </Typography>
        </CardContent>
        <CardActions sx={{ mb: "10px" }}>
          <AddToBasketButton {...props} />
        </CardActions>
      </Box>
    </Link>
  );
};

export default CatalogItem;
