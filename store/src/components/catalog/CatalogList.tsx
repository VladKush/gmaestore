import React, { useState, useEffect } from "react";
import { IGame } from "../types/Types";
import { useAppSelector } from "../../hooks/Hooks";
import CatalogItem from "./CatalogItem";
import DropdownGenres from "../dropdownGenres/DropdownGenres";
import { Container, Grid } from "@mui/material";
import SearchInput from "../searchInput/SearchInput";
import SortByPriceButton from "../sortByPriceButton/SortByPriceButton";
import Pagination from "../pagination/Pagination";
import { useNavigate } from "react-router-dom";
import "./CatalogList.scss";

const CatalogList: React.FC = () => {
  const games: IGame[] = useAppSelector((state) => state.games.games);
  const navigate = useNavigate();

  const [dropDownFilter, setDropDownFilter] = useState<[]>([]);
  const [searchGameInp, setSearchGameInp] = useState<string>("");
  const [sortPriseButton, setSortPriseButton] = useState<null>(null);

  //Pagination const
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [gamesPerePage] = useState<number>(6);

  //DropDownFilter settings
  const filteredGames = () => {
    return games.filter((game: IGame) => {
      const result = [];
      if (dropDownFilter.length > 0) {
        result.push(
          game.genres.filter((genre) => {
            return dropDownFilter.find(
              (selectedGenre: string) =>
                selectedGenre.toLowerCase() === genre.toLowerCase()
            );
          }).length > 0
        );
      }
      if (searchGameInp) {
        if (searchGameInp === "") {
          return games;
        }
        result.push(
          game.title.toLowerCase().includes(searchGameInp.toLowerCase())
        );
      }
      return !result.includes(false);
    });
  };

  useEffect(() => {
    setCurrentPage(1);
  }, [dropDownFilter]);

  //Pagination settings
  const lastGameIndex = currentPage * gamesPerePage;
  const firstGameIndex = lastGameIndex - gamesPerePage;
  const currentGame = filteredGames()
    .sort((a, b) => {
      if (sortPriseButton != null) {
        if (sortPriseButton) {
          return a.price > b.price ? 1 : -1;
        } else {
          return a.price > b.price ? -1 : 1;
        }
      } else return 0;
    })
    .slice(firstGameIndex, lastGameIndex);

  const pageCount = Math.ceil(filteredGames().length / gamesPerePage);
  const handlePageChange = ({ selected }: { selected: number }) => {
    setCurrentPage(selected + 1);
    navigate(`?page=${selected + 1}`);
  };

  return (
    <>
      <Container sx={{ mt: "40px", mb: "50px" }}>
        <div className="filters">
          <SearchInput setSearchGameInp={setSearchGameInp} />
          <SortByPriceButton
            setSortPriseButton={setSortPriseButton}
            sortPriseButton={sortPriseButton}
          />
          <DropdownGenres
            dropDownFilter={(value: []) => {
              setDropDownFilter(value);
            }}
          />
        </div>
      </Container>

      <Container>
        <Grid container spacing={3}>
          {currentGame.map((game) => {
            let pattern = new RegExp(`${searchGameInp}`, "gi");
            const title = game.title.replace(pattern, "<mark>$&</mark>");
            return (
              <Grid key={game.id} className="cart" item xs={12} md={6} xl={4}>
                <CatalogItem {...game} title={title} />
              </Grid>
            );
          })}
        </Grid>
        <Pagination
          forcePage={currentPage - 1}
          pageCount={pageCount}
          onChange={handlePageChange}
        />
      </Container>
    </>
  );
};

export default CatalogList;
