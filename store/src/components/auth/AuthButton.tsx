import React from "react";
import {Button} from "@mui/material";
import { useAuth0 } from "@auth0/auth0-react";
import NavUserMenu from "../navUserMenu/NavUserMenu";

const AuthButton: React.FC = () => {
  const { loginWithRedirect, isAuthenticated } = useAuth0();

  return (
    <>
      {!isAuthenticated ? (
        <Button
          onClick={() => loginWithRedirect()}
          color="primary"
          size="large"
        >
          LOGIN
        </Button>
      ) : (
        <NavUserMenu />
       )}
    </>
  );
};

export default AuthButton;
