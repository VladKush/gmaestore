import React from "react";
import { useAppSelector } from "../../hooks/Hooks";
import AddToBasketButton from "../addToBasketButton/AddToBasketButton";
import { Box, CardContent, Container, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import "./GamePage.scss";

const GamePage = () => {
  const game = useAppSelector((state) => state.gamePage.gamePage);
  const { title, video, description, genres, price } = game;

  return (
    <Container>
      <Link to="/">
        <Typography color="primary">

          <div className='back-arrow'>
            <ArrowBackIcon />
            <div>На главную</div>
          </div>

        </Typography>
      </Link>
      <Box>
        <CardContent>
          <Typography
            sx={{ mb: 3, mt: 5, textAlign: "center" }}
            variant="h4"
            color="primary"
            dangerouslySetInnerHTML={{ __html: title }}
          >
          </Typography>
          <iframe
            width="90%"
            height="500px"
            src={video}
            title="Youtube video Player"
            style={{ border: "none" }}
          />
          <Typography sx={{ mb: 3, mt: 3 }} color="primary" variant="h6">
            {description}
          </Typography>
          <Typography
            sx={{ mb: 2 }}
            color="primary"
            variant="body2"
            component="div"
          >
            Жарн: {genres.join(", ")}
          </Typography>
          <AddToBasketButton {...game} />
          <Typography
            sx={{ mt: 2 }}
            color="primary"
            variant="h6"
            component="div"
          >
            Цена: {price} грн.
          </Typography>
        </CardContent>
      </Box>
    </Container>
  );
};

export default GamePage;
